/**
 * Created by the Fab. Five on 26/05/15.
 */

app.controller('askFormCtrl',['$scope','$location', function($scope, $location){
    $scope.submitForm = function(event){
        console.log('Title:' + $scope.form.title);
        console.log('Question:' + $scope.form.question);
        console.log('Question:' + $scope.form.name);

	    var question = Parse.Object.extend("questions");
	    var Q = new question();
	    
	    Q.save({title: $scope.form.title, question: $scope.form.question, name: $scope.form.name, status: 0, personID: window.device.uuid});

	    $location.path("/startpage");
    };

}]);