app.controller('answerCtrl' ,["$scope", "$location", function($scope, $location){
    console.log("answerCtrl");
    
    var question = Parse.Object.extend("questions");
    var query = new Parse.Query(question);
    var qResult;
    query.equalTo("status", 0);
    query.ascending("createdAt");
    query.first().then(function(result) {
      console.log("result:");
      console.log(result);
      
      if(result._hasData){
        //sätt infon
          qResult = result;  //Behöver spara resultatet för att komma åt .id när vi sänder tebax svar.
          result.set("status", 1);
          result.save();

      }else{
        alert("No Qs available");
        $location.path("/startpage");
      }
      // only the selected fields of the object will now be available here.
      return result.fetch();
    });

    
    $scope.yes = function(){ 
      console.log("yes");     	  
    	$location.path("/startpage");
    }
    $scope.no = function(){  
      console.log("no"); 	   
    	$location.path("/startpage");
    }
    $scope.answer = function(){  
        console.log("answer");

        // Ändrar på kolumnen answere och sparar.
        qResult.set("answer", $scope.thetext);
        qResult.save(null, {
            success: function(point) {
                // Saved successfully.
            },
            error: function(point, error) {
                // The save failed.
                // error is a Parse.Error with an error code and description.
                console.log("Failed to save. Syntax error. Not DB error.");
            }
        });
        //Skickar kommando att starta Cloud Code "noticeUser"
        Parse.Cloud.run('noticeUser', {}, {
            success: function(result) {
                //Doing alright!
            },
            error: function(error) {
                Console.log("Couldn't do it.. Syntax error");
            }
        });

      $location.path("/startpage");
    }
     
    	
}]);