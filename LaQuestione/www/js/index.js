var app = angular.module('laQuestione', ['ngRoute']);

app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
        redirectTo: "/index.html"
    });

    $routeProvider.when("/startpage", {
        templateUrl: "startpage.html"
    });
    
    $routeProvider.when("/answer", {
        templateUrl: "answer.html"
    });

    $routeProvider.when("/askQuestion", {
        templateUrl: "askquestion.html"
    });   

    $routeProvider.when("/login", {
        templateUrl: "login.html"
    });

    $routeProvider.when("/animation", {
        templateUrl: "animation.html"
    });        
}]);

app.controller('contentCtrl', ['$scope', '$location', function($scope, $location){
    console.log("contentCtrl in index");
    Parse.initialize("7iBeKQfOokKRZttfaPOeOqU42XA8hj3D5KltSHyB", "DJtAzJfs8zpTE8idqcCOBhCbpJEUxsJPxd6G2YWT");
    $location.path("/animation");
}]);


